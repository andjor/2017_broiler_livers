## Diagnosis of Broiler Livers by Classifying Image Patches ##

This repo contains code developed for the paper presented at SCIA 2017.
The methods are implemented in python and C#. C# code is written in linqpad scripts, go to http://www.linqpad.net/ for download.

I am not at liberty to share the images recorded for and used in the paper, so you will need to supply you own images. I have uploaded a csv file with the features extracted from the probability maps and the neural net model trained by the patch classifier.

### External libraries ###

Tested with the following versions in Windows 10 64 bit.

- python 2.7.13 64 bit
    - theano 0.9.0
    - keras 1.2.0
    - numpy 1.11.3
    - scipy 0.18.1
    - scikit-learn 0.18.1
    - pillow 3.4.2

- C# 6.0
    - Emgu 3.1.0 http://www.emgu.com/wiki/index.php/Main_Page
    - SharpLearning https://github.com/mdabros/SharpLearning

### Procedure ###

1. Train the neural network patch classifier
    1. Run patch_net_trainer.py
    2. Test the nets performance with patch_predicter.py
2. Generate probability maps
    1. Run image_predicter.py
3. Extract features from probability maps
    1. Run ExtractFeaturesFromHeatmaps.linq
4. Train Random Forest classifier
    1. Run TrainLearnerToClassifyProbMaps_CrossValidation.linq
