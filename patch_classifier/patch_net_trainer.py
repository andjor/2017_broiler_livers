from os import walk, path, makedirs
import time
from msvcrt import kbhit, getch
import matplotlib.pyplot as plt
from keras.layers import (Activation, Convolution2D, Dense, Dropout, Flatten,
                          MaxPooling2D)
from keras.models import Sequential
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import CSVLogger
from keras.callbacks import ModelCheckpoint
from keras.layers.advanced_activations import LeakyReLU

# inputs
nb_epoch = 100
root_dir = r'**INSERT_PATH_HERE**'  # patch directory. Must contain a train and validation folder. 
nb_classes = 5

# dimensions of our images.
img_width, img_height = 75, 75

# training data
train_data_dir = path.join(root_dir, 'train')  # should contain a folder with images for each class
validation_data_dir = path.join(root_dir, 'validation')  # should contain a folder with images for each class

# model data
model_name = path.basename(path.normpath(root_dir))
model_name = 'scia2017_TestRun'
print('\n{}\n'.format(model_name))

if not path.exists('output/{}'.format(model_name)):
    makedirs('output/{}'.format(model_name))

val_acc_save_path = 'output/{}/val_acc.png'.format(model_name)
val_loss_save_path = 'output/{}/val_loss.png'.format(model_name)
model_checkpoint_path = 'output/{}/best_model.h5'.format(model_name)
metrics_path = 'output/{}/metrics.txt'.format(model_name)

nb_train_samples = 0
for path, subdirs, files in walk(train_data_dir):
    for name in files:
        nb_train_samples += 1

nb_validation_samples = 0
for path, subdirs, files in walk(validation_data_dir):
    for name in files:
        nb_validation_samples += 1

info = ['Model name: {}\n'.format(model_name),
        'Number of train samples: {}\n'.format(nb_train_samples),
        'Number of validation samples: {}\n'.format(nb_validation_samples),
        'Number of epochs: {}\n'.format(nb_epoch)]

# this is ugly
with open('output/' + model_name + '/' + model_name + '.txt', mode='w') as info_file:
    info_file.writelines(info)

# callbacks
csvlogger = CSVLogger('output/{}/training.log'.format(model_name), append=False)
modelcheckpoint = ModelCheckpoint(model_checkpoint_path,
                                  monitor='val_loss',
                                  verbose=1,
                                  save_best_only=True,
                                  save_weights_only=False,
                                  mode='auto')

leaky = LeakyReLU(0.1)

model = Sequential()
model.add(Convolution2D(32, 3, 3, input_shape=(3, img_width, img_height)))
model.add(LeakyReLU(0.1))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Convolution2D(32, 3, 3))
model.add(LeakyReLU(0.1))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Convolution2D(64, 3, 3))
model.add(LeakyReLU(0.1))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())
model.add(Dense(64))
model.add(LeakyReLU(0.1))
model.add(Dropout(0.5))
model.add(Dense(nb_classes))
model.add(Activation('softmax'))

model.compile(loss='categorical_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])


# plot(model, to_file='model.png')

# this is the augmentation configuration we will use for training
# train_datagen = ImageDataGenerator(
#         rescale=1./255,
#         shear_range=0.2,
#         zoom_range=0.2,
#         horizontal_flip=True)


# use this when training data is pre augmentated
train_datagen = ImageDataGenerator(rescale=1. / 255)

val_datagen = ImageDataGenerator(rescale=1. / 255)

train_generator = train_datagen.flow_from_directory(train_data_dir,
    target_size=(img_width, img_height),
    batch_size=64,
    class_mode='categorical')

validation_generator = val_datagen.flow_from_directory(validation_data_dir,
    target_size=(img_width, img_height),
    batch_size=64,
    class_mode='categorical')

loss = list()
val_loss = list()
acc = list()
val_acc = list()

start = time.clock()

for i in range(nb_epoch):
    print('{} of {} epochs'.format(i, nb_epoch))
    history = model.fit_generator(train_generator,
        samples_per_epoch=nb_train_samples,
        nb_epoch=1,
        validation_data=validation_generator,
        nb_val_samples=nb_validation_samples,
        callbacks=[modelcheckpoint])
    loss.append(history.history['loss'][0])
    val_loss.append(history.history['val_loss'][0])
    acc.append(history.history['acc'][0])
    val_acc.append(history.history['val_acc'][0])
    if kbhit():
        getch()
        print('Training aborted by user. Cleaning up.')
        break

stop = time.clock()

fig = plt.figure()
plt.plot(loss)
plt.plot(val_loss)
# plt.title('Model Loss')
plt.xlabel('Number of epochs')
plt.ylabel('Loss')
plt.legend(['train', 'validation'], loc='upper right')
fig.savefig(val_loss_save_path)

fig = plt.figure()
plt.plot(acc)
plt.plot(val_acc)
# plt.title('Model Accuracy')
plt.xlabel('Number of epochs')
plt.ylabel('Accuracy')
plt.legend(['train', 'validation'], loc='lower right')
fig.savefig(val_acc_save_path)

with open(metrics_path, mode='w') as metric_file:
    metric_file.write('acc,' + ','.join(format(x, "10.3f") for x in acc) + '\n')
    metric_file.write('val_acc,' + ','.join(format(x, "10.3f") for x in val_acc) + '\n')
    metric_file.write('loss,' + ','.join(format(x, "10.3f") for x in loss) + '\n')
    metric_file.write('val_loss,' + ','.join(format(x, "10.3f") for x in val_loss) + '\n')

info = ['Model classes: {}\n'.format(train_generator.class_indices)]

with open('output/' + model_name + '/' + model_name + '.txt', mode='a') as info_file:
    info_file.writelines(info)

print('Training took {} seconds'.format(stop - start))
