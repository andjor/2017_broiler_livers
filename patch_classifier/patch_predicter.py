import glob
import numpy as np
from sklearn.metrics import confusion_matrix
from PIL import Image
from keras.models import load_model
from os import path, makedirs

print('Predict patches')

good_path = r'**INSERT_PATH_HERE**\0_healthy\*.png'  # test patches
cobble_path = r'**INSERT_PATH_HERE**\1_cobble\*.png'  # test patches
white_path = r'**INSERT_PATH_HERE**\2_white\*.png'  # test patches
inflam_path = r'**INSERT_PATH_HERE**\3_inflam\*.png'  # test patches
misc_path = r'**INSERT_PATH_HERE**\4_misc\*.png'  # test patches

model_name = 'scia2017_TestRun'

files = glob.glob(good_path)
files.extend(glob.glob(cobble_path))
files.extend(glob.glob(white_path))
files.extend(glob.glob(inflam_path))
files.extend(glob.glob(misc_path))

print('\n'.join(files))

images = np.asarray([np.reshape(np.asarray(Image.open(i), dtype=np.float).T * 1./255, (3, 75, 75)) for i in files])

samples = len(files) / 5

y_true = np.array([0 for i in range(samples)] +
                  [1 for i in range(samples)] +
                  [2 for i in range(samples)] +
                  [3 for i in range(samples)] +
                  [4 for i in range(samples)])
print(y_true)

model = load_model('../data/patch_model.h5'.format(model_name))

res = model.predict_proba(images, batch_size=20, verbose=0)

y_pred = np.argmax(res, axis=1)

print(np.max(res, axis=1))

conf = confusion_matrix(y_true, y_pred)

print(conf)

if not path.exists('output/{}'.format(model_name)):
    makedirs('output/{}'.format(model_name))

np.savetxt('output/{}/confusion_matrix.txt'.format(model_name), conf, fmt='%d')

for i in range(len(y_pred)):
    if y_pred[i] != y_true[i]:
        print(files[i])