from collections import namedtuple
from os.path import join, splitext, exists, basename
from os import makedirs
import time
from PIL import Image
import numpy as np
import matplotlib.image as mpimg
from keras.models import load_model
import glob
import ImgProc

Pair = namedtuple('Pair', 'bgr mask')


def write_features_to_csv(features, output_folder):

    output_file = join(output_folder, 'features.txt')
    file = open(output_file, 'w')

    with open(output_file, mode='w') as file:
        file.write(','.join([str(i) for i in features]))


def save_byte_image(image, output_folder, filename):
    temp = (image * 255.0).astype(np.uint8)
    Image.fromarray(temp).save(join(output_folder, '{}.png'.format(filename)))


def predict_image(image, model, patch_size=(75, 75), step=10):
    assert isinstance(image, np.ndarray)

    height, width, channels = image.shape
    pw = patch_size[1]
    ph = patch_size[0]

    yrange = range(0, height - ph, step)
    xrange = range(0, width - pw, step)

    sw = len(xrange)
    sh = len(yrange)

    good_map = np.zeros([sh, sw], dtype=np.float32)
    cobble_map = np.zeros([sh, sw], dtype=np.float32)
    white_map = np.zeros([sh, sw], dtype=np.float32)
    inflam_map = np.zeros([sh, sw], dtype=np.float32)
    misc_map = np.zeros([sh, sw], dtype=np.float32)

    sy = 0
    for y in yrange:
        sx = 0
        images_in_line = np.asarray(
            [np.reshape(image[y: y + ph, x: x + pw].T, (3, pw, ph))
                for x in xrange])
        output = model.predict_proba(
            images_in_line, batch_size=np.size(images_in_line, 0), verbose=0)

        for x in range(0, len(output)):
            good_map[sy, sx] = output[x, 0]
            cobble_map[sy, sx] = output[x, 1]
            white_map[sy, sx] = output[x, 2]
            inflam_map[sy, sx] = output[x, 3]
            misc_map[sy, sx] = output[x, 4]

            sx += 1
        sy += 1

    return good_map, cobble_map, white_map, inflam_map, misc_map


def predict_pair(bgrpath, maskpath, model, output_folder):
    assert isinstance(bgrpath, str)
    assert isinstance(maskpath, str)

    # _image -- numpy array with pixel values
    # _mask -- numpy array with binary values
    # _roi -- Roi element

    patch_size = (75, 75)
    step = 5  # move 5 pixels between every prediction
    hl_ratio = 0.1  # max 10 % highlight alowed

    bgr_image = mpimg.imread(bgrpath)

    # ceil here as mask images have a value of 150 where there is liver
    liver_mask = np.ceil(mpimg.imread(maskpath)).astype(int)

    highlight_mask = ImgProc.highlight_remove.create_highlight_mask(bgr_image).astype(int)

    mask_roi = ImgProc.roi.get_roi(liver_mask, patch_size[0] / 2)

    weights_image = ImgProc.highlight_remove.get_weight_mask(highlight_mask,
                                                             liver_mask,
                                                             roi=mask_roi,
                                                             patch_size=patch_size,
                                                             step=step,
                                                             max_HL_ratio=hl_ratio)

    liver_image = bgr_image[
        mask_roi.miny:mask_roi.maxy, mask_roi.minx:mask_roi.maxx, :]

    good_image, cobble_image, white_image, inflam_image, misc_image = predict_image(
        liver_image, model, step=step)

    good_w_image = good_image * weights_image
    cobble_w_image = cobble_image * weights_image
    white_w_image = white_image * weights_image
    inflam_w_image = inflam_image * weights_image
    misc_w_image = misc_image * weights_image

    liver_only_image = np.copy(bgr_image)
    liver_only_image[liver_mask < 0.5] = 0

    save_byte_image(highlight_mask, output_folder, 'highlight_mask')
    save_byte_image(liver_only_image, output_folder, 'liver_only_image')
    save_byte_image(liver_mask, output_folder, 'liver_mask')
    save_byte_image(liver_image, output_folder, 'liver_image')
    save_byte_image(weights_image, output_folder, 'weights_image')
    save_byte_image(good_w_image, output_folder, 'w_good_image')
    save_byte_image(cobble_w_image, output_folder, 'w_cobble_image')
    save_byte_image(white_w_image, output_folder, 'w_white_image')
    save_byte_image(inflam_w_image, output_folder, 'w_inflam_image')
    save_byte_image(misc_w_image, output_folder, 'w_misc_image')
    save_byte_image(misc_image, output_folder, 'misc_image')


def get_filepairs_from_folder(path):
    assert isinstance(path, str)

    image_folder = 'CROP_IMAGES'
    liver_mask_folder = 'LIVER_MASKS'

    bgr_files = glob.glob('{}/*.png'.format(join(path, image_folder)))
    mask_files = glob.glob('{}/*.png'.format(join(path, liver_mask_folder)))

    return [Pair(i[0], i[1]) for i in zip(bgr_files, mask_files)]


def get_filepairs_with_search(inputfolder, maskfolder):
    assert isinstance(inputfolder, str)
    assert isinstance(maskfolder, str)

    bgr_files = glob.glob('{}/*.png'.format(inputfolder))
    mask_files = glob.glob('{}/*.png'.format(maskfolder))

    return [Pair(i, [s for s in mask_files if basename(i) in s][0]) for i in bgr_files]


if __name__ == '__main__':

    class_path = r'**INSERT_PATH_HERE**\0_healthy'  # path to full scale images from one class
    mask_path = r'**INSERT_PATH_HERE**'  # path to masks with the liver regions
    model_path = r'..\data\patch_model.h5'  # best model from training the patch classifier
    target = '0_healthy'  # friendly name for output folder

    print(target)

    model = load_model(model_path)

    output_folder = '.\output_pic\scia2017_TestRun'
    output_folder = join(output_folder, target)

    imagepairs = get_filepairs_with_search(class_path, mask_path)
    length = len(imagepairs)

    for i in range(length):
        starttime = time.time()

        imagepair = imagepairs[i]
        image_name = splitext(basename(imagepair.bgr))[0]
        print('Processing image {} of {} : {}'.format(i+1, length, image_name))

        image_folder_path = join(output_folder, image_name)
        if not exists(image_folder_path):
            makedirs(image_folder_path)
        predict_pair(imagepair.bgr, imagepair.mask, model, image_folder_path)
        print('Took {} seconds'.format(time.time() - starttime))
