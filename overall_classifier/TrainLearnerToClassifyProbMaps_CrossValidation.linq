<Query Kind="Program">
  <Reference>C:\lib\Emgu\emgucv-windesktop 3.1.0.2282\bin\Emgu.CV.UI.dll</Reference>
  <Reference>C:\lib\Emgu\emgucv-windesktop 3.1.0.2282\bin\Emgu.CV.World.dll</Reference>
  <Reference>D:\IHFood\SharpLearningExample\SharpLearning\SharpLearning.AdaBoost.dll</Reference>
  <Reference>D:\IHFood\SharpLearningExample\SharpLearning\SharpLearning.Common.Interfaces.dll</Reference>
  <Reference>D:\IHFood\SharpLearningExample\SharpLearning\SharpLearning.Containers.dll</Reference>
  <Reference>D:\IHFood\SharpLearningExample\SharpLearning\SharpLearning.CrossValidation.dll</Reference>
  <Reference>D:\IHFood\SharpLearningExample\SharpLearning\SharpLearning.DecisionTrees.dll</Reference>
  <Reference>D:\IHFood\SharpLearningExample\SharpLearning\SharpLearning.GradientBoost.dll</Reference>
  <Reference>D:\IHFood\SharpLearningExample\SharpLearning\SharpLearning.InputOutput.dll</Reference>
  <Reference>D:\IHFood\SharpLearningExample\SharpLearning\SharpLearning.Linear.dll</Reference>
  <Reference>D:\IHFood\SharpLearningExample\SharpLearning\SharpLearning.Metrics.dll</Reference>
  <Reference>D:\IHFood\SharpLearningExample\SharpLearning\SharpLearning.Optimization.dll</Reference>
  <Reference>D:\IHFood\SharpLearningExample\SharpLearning\SharpLearning.RandomForest.dll</Reference>
  <Reference>D:\IHFood\SharpLearningExample\SharpLearning\SharpLearning.Threading.dll</Reference>
  <Namespace>Emgu.CV</Namespace>
  <Namespace>Emgu.CV.Structure</Namespace>
  <Namespace>Emgu.CV.UI</Namespace>
  <Namespace>Emgu.CV.Util</Namespace>
  <Namespace>SharpLearning.InputOutput.Csv</Namespace>
  <Namespace>SharpLearning.Linear.Learners</Namespace>
  <Namespace>SharpLearning.DecisionTrees.Learners</Namespace>
  <Namespace>System</Namespace>
  <Namespace>SharpLearning.CrossValidation.CrossValidators</Namespace>
  <Namespace>SharpLearning.Metrics.Regression</Namespace>
  <Namespace>SharpLearning.Metrics.Classification</Namespace>
  <Namespace>SharpLearning.CrossValidation.TrainingTestSplitters</Namespace>
  <Namespace>System.Globalization</Namespace>
  <Namespace>System.Drawing</Namespace>
  <Namespace>Emgu.CV.CvEnum</Namespace>
</Query>

void Main()
{
	Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
	Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
	Util.AutoScrollResults = true;

	// Run n times
	int n = 10;

	var overallFeatureFile = @"..\data\overall_features.csv";

	var features = new[] { "FEA_CobbleSum", "FEA_GoodSobRatio", "FEA_WhiteSumPart", "FEA_MiscSumPart", "FEA_CobbleRatio", "FEA_InflamHighPart", "FEA_InflamHighCount", "FEA_GoodHighPart", "FEA_MiscRatio", "FEA_GoodRatio" };

	// Use StreamWriter(filepath) when running from filesystem
	var parser = new CsvParser(() => new StreamReader(overallFeatureFile), ',');

	// read features
	var observations = parser.EnumerateRows(f => f.StartsWith("FEA")).ToF64Matrix();
	var targets = parser.EnumerateRows("GT_Target").ToF64Vector();
	var ids = parser.EnumerateRows("AUX_ID").ToStringVector();

	var avgErrors = new double[n];
	var errors = new double[n];
	var rand = new Random();

	var confMatrix = new int[4, 4, n];

	for (int i = 0; i < n; i++)
	{
		// create learner
		var looplearner = new SharpLearning.RandomForest.Learners.ClassificationRandomForestLearner();
		var loopcrossval = new SharpLearning.CrossValidation.CrossValidators.StratifiedCrossValidation<double>(crossValidationFolds: 5, seed: rand.Next());
		var looppredictions = loopcrossval.CrossValidate(looplearner, observations, targets);
		
		for (int j = 0; j < targets.Length; j++)
		{
			var t = (int)targets[j];
			var p = (int)looppredictions[j];
			
			confMatrix[t,p,i]++;
		}

		// measure the error
		var metric = new SharpLearning.Metrics.Classification.TotalErrorClassificationMetric<double>();
		errors[i] = metric.Error(targets, looppredictions).Dump($"Run {i} classification error");
		avgErrors[i] = errors.Take(i + 1).Average(f => f).Dump($"Avg Error{avgErrors[i]}");
		var errorString = metric.ErrorString(targets, looppredictions).Dump($"Run {i} Classification Matrix");
	}

	$"{errors.Average(f => f)}".Dump("Average Classification Error");

	errors.Dump("classsification error");
	errors.Average().Dump("Avg classification error");
	StdDev(errors).Dump("stdev");
	
	for (int i = 0; i < n; i++)
	{
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				Console.Write($"{confMatrix[y, x, i]},");
			}
			Console.WriteLine();
		}
		Console.WriteLine();
	}

	var values = new double[n];
	for (int y = 0; y < 4; y++)
	{
		for (int x = 0; x < 4; x++)
		{
			
			for (int i = 0; i < n; i++)
			{
				values[i] = confMatrix[y, x, i];
			}
			
			var avg = values.Average();
			var std = StdDev(values);
			Console.Write($"{avg} + {std:0.00}\t");
		}
		Console.WriteLine();
	}
}

// Define other methods and classes here
public static double StdDev(IEnumerable<double> values)
{
	// ref: http://warrenseen.com/blog/2006/03/13/how-to-calculate-standard-deviation/
	double mean = 0.0;
	double sum = 0.0;
	double stdDev = 0.0;
	int n = 0;
	foreach (double val in values)
	{
		n++;
		double delta = val - mean;
		mean += delta / n;
		sum += delta * (val - mean);
	}
	if (1 < n)
		stdDev = Math.Sqrt(sum / (n - 1));

	return stdDev;
}