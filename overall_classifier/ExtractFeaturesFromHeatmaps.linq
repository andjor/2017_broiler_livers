<Query Kind="Program">
  <Reference>C:\lib\Emgu\emgucv-windesktop 3.1.0.2282\bin\Emgu.CV.UI.dll</Reference>
  <Reference>C:\lib\Emgu\emgucv-windesktop 3.1.0.2282\bin\Emgu.CV.World.dll</Reference>
  <Namespace>Emgu.CV</Namespace>
  <Namespace>Emgu.CV.Structure</Namespace>
  <Namespace>Emgu.CV.UI</Namespace>
  <Namespace>Emgu.CV.Util</Namespace>
  <Namespace>System.Globalization</Namespace>
</Query>

void Main()
{
	Util.AutoScrollResults = true;

	Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

	var rootfolder = @"**OUTPUT_FOLDER_FROM_image_predicter.py**";

	var folders = Directory.GetDirectories(rootfolder);
	var featureAllFile = Path.Combine(rootfolder, $"features_all.csv");
	var featureTrainFile = Path.Combine(rootfolder, $"features_train.csv");
	var featureTestFile = Path.Combine(rootfolder, $"features_test.csv");

	var targetDict = new Dictionary<string, double>();
	targetDict.Add("0_healthy", 0);
	targetDict.Add("1_cobble", 1);
	targetDict.Add("2_white", 2);
	targetDict.Add("3_inflam", 3);

	var table = new DataTable($"Features");
	table.Columns.Add("GT_Target", typeof(double));
	table.Columns.Add("FEA_GoodSum", typeof(double));
	table.Columns.Add("FEA_GoodCount", typeof(double));
	table.Columns.Add("FEA_GoodRatio", typeof(double));
	table.Columns.Add("FEA_GoodPart", typeof(double));
	table.Columns.Add("FEA_GoodSumPart", typeof(double));
	table.Columns.Add("FEA_GoodHighCount", typeof(double));
	table.Columns.Add("FEA_GoodHighPart", typeof(double));
	table.Columns.Add("FEA_GoodSobRatio", typeof(double));
	table.Columns.Add("FEA_CobbleSum", typeof(double));
	table.Columns.Add("FEA_CobbleCount", typeof(double));
	table.Columns.Add("FEA_CobbleRatio", typeof(double));
	table.Columns.Add("FEA_CobblePart", typeof(double));
	table.Columns.Add("FEA_CobbleSumPart", typeof(double));
	table.Columns.Add("FEA_CobbleHighCount", typeof(double));
	table.Columns.Add("FEA_CobbleHighPart", typeof(double));
	table.Columns.Add("FEA_CobbleSobRatio", typeof(double));
	table.Columns.Add("FEA_WhiteSum", typeof(double));
	table.Columns.Add("FEA_WhiteCount", typeof(double));
	table.Columns.Add("FEA_WhiteRatio", typeof(double));
	table.Columns.Add("FEA_WhitePart", typeof(double));
	table.Columns.Add("FEA_WhiteSumPart", typeof(double));
	table.Columns.Add("FEA_WhiteHighCount", typeof(double));
	table.Columns.Add("FEA_WhiteHighPart", typeof(double));
	table.Columns.Add("FEA_WhiteSobRatio", typeof(double));
	table.Columns.Add("FEA_InflamSum", typeof(double));
	table.Columns.Add("FEA_InflamCount", typeof(double));
	table.Columns.Add("FEA_InflamRatio", typeof(double));
	table.Columns.Add("FEA_InflamPart", typeof(double));
	table.Columns.Add("FEA_InflamSumPart", typeof(double));
	table.Columns.Add("FEA_InflamHighCount", typeof(double));
	table.Columns.Add("FEA_InflamHighPart", typeof(double));
	table.Columns.Add("FEA_InflamSobRatio", typeof(double));
	table.Columns.Add("FEA_MiscSum", typeof(double));
	table.Columns.Add("FEA_MiscCount", typeof(double));
	table.Columns.Add("FEA_MiscRatio", typeof(double));
	table.Columns.Add("FEA_MiscPart", typeof(double));
	table.Columns.Add("FEA_MiscSumPart", typeof(double));
	table.Columns.Add("FEA_MiscHighCount", typeof(double));
	table.Columns.Add("FEA_MiscHighPart", typeof(double));
	table.Columns.Add("FEA_MiscSobRatio", typeof(double));
	table.Columns.Add("AUX_ID", typeof(string));

	var datalist = new List<Observation>();
	
	var goodImageName = @"w_good_image.png";
	var cobbleImageName = @"w_cobble_image.png";
	var whiteImageName = @"w_white_image.png";
	var inflamImageName = @"w_inflam_image.png";
	var miscImageName = @"w_misc_image.png";

	foreach (var classfolder in folders)
	{
		var targetFolder = classfolder;
		var target = Path.GetFileName(targetFolder).Dump("target folder");

		var productFolders = Directory.GetDirectories(targetFolder);

		var black = new Gray(0);
		var white = new Gray(255);
		var high = new Gray(0.8 * 255.0);

		foreach (var folder in productFolders)
		{
			var id = Path.GetFileName(folder);

			if (!File.Exists(Path.Combine(folder, goodImageName))) break;
			if (!File.Exists(Path.Combine(folder, cobbleImageName))) break;
			if (!File.Exists(Path.Combine(folder, whiteImageName))) break;
			if (!File.Exists(Path.Combine(folder, inflamImageName))) break;
			if (!File.Exists(Path.Combine(folder, miscImageName))) break;

			using (var goodImage = new Image<Gray, byte>(Path.Combine(folder, goodImageName)))
			using (var cobbleImage = new Image<Gray, byte>(Path.Combine(folder, cobbleImageName)))
			using (var whiteImage = new Image<Gray, byte>(Path.Combine(folder, whiteImageName)))
			using (var inflamImage = new Image<Gray, byte>(Path.Combine(folder, inflamImageName)))
			using (var miscImage = new Image<Gray, byte>(Path.Combine(folder, miscImageName)))
			using (var goodSobImage = goodImage.Sobel(1, 0, 3).AddWeighted(goodImage.Sobel(0, 1, 3), 0.5, 0.5, 0).ThresholdToZero(black))
			using (var cobbleSobImage = cobbleImage.Sobel(1, 0, 3).AddWeighted(cobbleImage.Sobel(0, 1, 3), 0.5, 0.5, 0).ThresholdToZero(black))
			using (var whiteSobImage = whiteImage.Sobel(1, 0, 3).AddWeighted(whiteImage.Sobel(0, 1, 3), 0.5, 0.5, 0).ThresholdToZero(black))
			using (var inflamSobImage = inflamImage.Sobel(1, 0, 3).AddWeighted(inflamImage.Sobel(0, 1, 3), 0.5, 0.5, 0).ThresholdToZero(black))
			using (var miscSobImage = miscImage.Sobel(1, 0, 3).AddWeighted(miscImage.Sobel(0, 1, 3), 0.5, 0.5, 0).ThresholdToZero(black))
			using (var goodThImage = goodImage.ThresholdBinary(black, white))
			using (var cobbleThImage = cobbleImage.ThresholdBinary(black, white))
			using (var whiteThImage = whiteImage.ThresholdBinary(black, white))
			using (var inflamThImage = inflamImage.ThresholdBinary(black, white))
			using (var miscThImage = miscImage.ThresholdBinary(black, white))
			using (var goodHighImage = goodImage.ThresholdBinary(high, white))
			using (var cobbleHighImage = cobbleImage.ThresholdBinary(high, white))
			using (var whiteHighImage = whiteImage.ThresholdBinary(high, white))
			using (var inflamHighImage = inflamImage.ThresholdBinary(high, white))
			using (var miscHighImage = miscImage.ThresholdBinary(high, white))
			{
				var h = goodImage.Height;
				var w = goodImage.Width;

				var goodSum = goodImage.GetSum().Intensity;
				var goodCount = goodThImage.GetSum().Intensity / 255;
				var goodHighCount = goodHighImage.GetSum().Intensity / 255;
				var goodRatio = goodSum / goodCount;
				var goodSobRatio = goodSobImage.GetSum().Intensity / goodCount;
				var cobbleSum = cobbleImage.GetSum().Intensity;
				var cobbleCount = cobbleThImage.GetSum().Intensity / 255;
				var cobbleHighCount = cobbleHighImage.GetSum().Intensity / 255;
				var cobbleRatio = cobbleSum / cobbleCount;
				var cobbleSobRatio = cobbleSobImage.GetSum().Intensity / cobbleCount;
				var whiteSum = whiteImage.GetSum().Intensity;
				var whiteCount = whiteThImage.GetSum().Intensity / 255;
				var whiteHighCount = whiteHighImage.GetSum().Intensity / 255;
				var whiteRatio = whiteSum / whiteCount;
				var whiteSobRatio = whiteSobImage.GetSum().Intensity / whiteCount;
				var inflamSum = inflamImage.GetSum().Intensity;
				var inflamCount = inflamThImage.GetSum().Intensity / 255;
				var inflamHighCount = inflamHighImage.GetSum().Intensity / 255;
				var inflamRatio = inflamSum / inflamCount;
				var inflamSobRatio = inflamSobImage.GetSum().Intensity / inflamCount;
				var miscSum = miscImage.GetSum().Intensity;
				var miscCount = miscThImage.GetSum().Intensity / 255;
				var miscHighCount = miscHighImage.GetSum().Intensity / 255;
				var miscRatio = miscSum / miscCount;
				var miscSobRatio = miscSobImage.GetSum().Intensity / miscCount;

				var totalCount = w * h;
				
				var goodPart = goodCount / totalCount;
				var goodSumPart = goodSum / totalCount;
				var goodHighPart = goodHighCount / totalCount;
				var cobblePart = cobbleCount / totalCount;
				var cobbleSumPart = cobbleSum / totalCount;
				var cobbleHighPart = cobbleHighCount / totalCount;
				var whitePart = whiteCount / totalCount;
				var whiteSumPart = whiteSum / totalCount;
				var whiteHighPart = whiteHighCount / totalCount;
				var inflamPart = inflamCount / totalCount;
				var inflamSumPart = inflamSum / totalCount;
				var inflamHighPart = inflamHighCount / totalCount;
				var miscPart = miscCount / totalCount;
				var miscSumPart = miscSum / totalCount;
				var miscHighPart = miscHighCount / totalCount;

				datalist.Add(new Observation(targetDict[target],
											 new[] {goodSum, goodCount, goodRatio, goodPart, goodSumPart, goodHighCount, goodHighPart, goodSobRatio,
												cobbleSum, cobbleCount, cobbleRatio, cobblePart, cobbleSumPart, cobbleHighCount, cobbleHighPart, cobbleSobRatio,
												whiteSum, whiteCount, whiteRatio, whitePart, whiteSumPart, whiteHighCount, whiteHighPart, whiteSobRatio,
												inflamSum, inflamCount, inflamRatio, inflamPart, inflamSumPart, inflamHighCount, inflamHighPart, inflamSobRatio,
												miscSum, miscCount, miscRatio, miscPart, miscSumPart, miscHighCount, miscHighPart, miscSobRatio},
												id));

				CvInvoke.Imshow(nameof(goodSobImage), goodSobImage);
				CvInvoke.Imshow(nameof(goodImage), goodImage);
				CvInvoke.Imshow(nameof(goodThImage), goodThImage);
				CvInvoke.Imshow(nameof(goodHighImage), goodHighImage);
				var key = (char)CvInvoke.WaitKey(10);
				if (key == 'q') break;
			}
		}
	}

	CvInvoke.DestroyAllWindows();
	
	// Train features
	//WriteCsvFile(datalist, table, targetDict, featureTrainFile, 20, -1);

	// Test features
	//WriteCsvFile(datalist, table, targetDict, featureTestFile, 0, 20);

	// All features
	WriteCsvFile(datalist, table, targetDict, featureAllFile, 0, -1);
}

// Define other methods and classes here
public void WriteCsvFile(List<Observation> obslist, DataTable table, Dictionary<string, double> targetdict, string file, int rows2skip, int rows2take = -1)
{
	var temptable = table.Copy();
	foreach (var target in targetdict.Values)
	{
		var targetlist = rows2take < 0 ? obslist.Where(f => f.Target == target).Skip(rows2skip) : obslist.Where(f => f.Target == target).Skip(rows2skip).Take(rows2take);
		foreach (var obs in targetlist)
		{
			var arr = new List<object>();
			arr.Add(obs.Target);
			arr.AddRange(obs.Measurements.Cast<object>());
			arr.Add(obs.ID);
			temptable.Rows.Add(arr.ToArray());
		}
	}
	Util.WriteCsv(temptable, file, true);
}


public class Observation
{
	public double Target { get; set; }
	public double[] Measurements { get; set; }
	public string ID { get; set; }

	public Observation(double target, double[] measurements, string id)
	{
		Target = target;
		Measurements = measurements;
		ID = id;
	}
}