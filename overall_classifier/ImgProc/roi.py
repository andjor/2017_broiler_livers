from numba import jit
from structs import Roi


@jit
def get_roi(image, border_width):
    # assert isinstance(image, np.ndarray)
    # assert isinstance(border_width, int)

    h, w = image.shape
    minx = 2000
    miny = 2000
    maxx = 0
    maxy = 0

    for y in range(0, h):
        for x in range(0, w):
            val = image[y, x]
            if (x < minx and val > 0):
                minx = x
            if (y < miny and val > 0):
                miny = y
            if (x > maxx and val > 0):
                maxx = x
            if (y > maxy and val > 0):
                maxy = y

    # print('minx: {}, maxx: {}, miny: {}, mmaxy: {}'.format(minx, maxx, miny, maxy))

    minx = max(0, minx - border_width)
    miny = max(0, miny - border_width)
    maxx = min(w - 1, maxx + border_width)
    maxy = min(h - 1, maxy + border_width)

    return Roi(minx, miny, maxx, maxy)
