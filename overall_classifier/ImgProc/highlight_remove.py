import numpy as np
import structs
from numba import jit


@jit
def create_highlight_mask(image):
    # type: (np.ndarray) -> np.ndarray
    """Create binary highlight mask

    Keyword arguments:
    image -- 2D numpy array
    """
    assert isinstance(image, np.ndarray)

    bgr = image
    r = bgr[:, :, 0]
    g = bgr[:, :, 1]
    b = bgr[:, :, 2]
    mask = np.ones(b.shape)
    bt = (200. / 255)
    gt = (175. / 255)

    temp = np.logical_or(np.logical_and(b >= r, b > bt), g > gt)
    temp = np.logical_or(temp, np.logical_and(g >= r, g >= b))
    temp = np.logical_or(temp, r + b + g == 0)
    mask[temp] = 0

    return mask


@jit
def create_weight_mask(mask, patch_size, max_HL_ratio=0.1):
    # type: (np.ndarray, tuple) -> np.ndarray
    """Create weight mask

    Keyword arguments:
    mask -- 2D numpy array
    patch_size -- (x,y) tuple with patch size
    max_HL_ratio -- maximum allowed HighLight ratio
    """

    w = mask.shape[0]
    h = mask.shape[1]
    pw = patch_size[0]
    ph = patch_size[1]

    pwh = pw / 2
    phh = ph / 2

    max_highlight = max_HL_ratio * pw * ph
    weights = np.zeros(mask.shape, dtype=np.float32)

    for y in range(phh, h - phh):
        for x in range(pwh, w - pwh):
            patch = mask[x - pwh:x + pwh + 1, y - phh:y + phh + 1]
            patch_white_count = np.sum(patch)
            patch_black_count = pw * ph - patch_white_count

            rev = max_highlight - patch_black_count

            if rev < 0:
                weights[x, y] = 0
            else:
                weights[x, y] = rev / max_highlight

    return weights


@jit
def get_weight_mask(hlmask, objectmask, roi, patch_size, step=10, max_HL_ratio=0.1):
    """Create weight mask

    Keyword arguments:
    hlmask -- highlight mask, 2D numpy array
    objectmask -- organ mask, 2D numpy array
    roi -- roi around the liver area, Roi (namedTuple)
    patch_size -- (x,y) tuple with patch size
    setp -- step 10 pixels every run
    max_HL_ratio -- maximum allowed HighLight ratio
    """
    assert isinstance(hlmask, np.ndarray)
    assert isinstance(objectmask, np.ndarray)
    assert isinstance(roi, structs.Roi)

    if (hlmask.shape != objectmask.shape):
        raise AssertionError()

    pw = patch_size[0]
    ph = patch_size[1]

    max_highlight = max_HL_ratio * pw * ph

    xrange = range(roi.minx, roi.maxx - pw, step)
    yrange = range(roi.miny, roi.maxy - ph, step)
    sw = len(xrange)
    sh = len(yrange)
    weights = np.zeros((sh, sw), dtype=float)

    sy = 0
    for y in yrange:
        sx = 0
        for x in xrange:

            hl_patch = hlmask[y:y + ph, x:x + pw]
            object_patch = objectmask[y:y + ph, x:x + pw]

            temp_patch = np.bitwise_and(hl_patch, object_patch)

            patch_white_count = np.sum(temp_patch)
            patch_black_count = (pw * ph) - patch_white_count
            rev = max_highlight - patch_black_count

            weights[sy, sx] = 0 if (rev < 0) else (rev / max_highlight)
            sx += 1
        sy += 1

    return weights
