from collections import namedtuple

Roi = namedtuple('Roi', 'minx miny maxx maxy')